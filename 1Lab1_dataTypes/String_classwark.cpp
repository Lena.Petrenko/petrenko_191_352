﻿//#include "stdafx.h"
#include<stdio.h>
#include "pch.h"
#include<cstring> // strcat(), strlen(), strcmp()
// C-строки - это MACCИВ сиволов с окончанием '\0'

#include <string>
// c++ строки - это класс
#include <iostream>
#include <fstream>
int main()
{

	char cstr1[] = "C-string 1";
	char cstr2[] = { 'C',' ','-',' ','s','t','r','i','n','g',' ','1','\0' };
	char cstr3[256] = { '\67' };
	//таблица соответствия символов - ASCII, ANSI, UNICODE

	printf_s("Hello, world!\n int = %d, double = %8.4f, char = %c\n, %s\n", 5, 3.4, 't',cstr2);
	printf_s(cstr1);

	scanf_s(cstr3);
	//strcpy - копирование
	//strcat - конкатенация
	//strcmp - сравнение
	//strlen - длина строки

	strcpy_s(cstr3, cstr1);
	printf("\nstrcpy(cstr3, cstr1); printf(cstr3) = %s\n", cstr3);
	strcat_s(cstr3, cstr1);
	printf_s("\nstrcat_s(cstr3, cstr1); printf(cstr3) = %s\n", cstr3);
	printf_s("\nstrcmp(cstr3, cstr1); %d", strcmp(cstr1, cstr1));
	printf_s("\nstrlen(cstr1) = %d strlen(cstr3) =  %d\n", strlen(cstr1), strlen(cstr3));



	std::string str1 = "C++ string 1";
	std::string str2;
	str2 = str1;
	std::cout << "str2 = str1;" << str2 << std::endl;
	std::cout << "str2+str1;" << str2 + str1 << std::endl;
	str2 += str1;
	std::cout << "str2 += str1;str2>str1 = " << (str2 > str1) << std::endl;
	std::cout << "str1.length()" << str1.length() << std::endl;

	std::cout << "\nstr2.find (\"++\");" << str2.find("++");
	std::cout << "\nstr2.find_first_of(\"igs\");" << str2.find_first_of("igs");

	str2.insert(10, "12345");
	std::cout << "str2.insert(10, \"12345\"); str2 =" << str2 << std::endl;

	str2.replace(10, 2, "qwerty");
	std::cout << "str2.replace(10, 2, \"qwerty\"); str2 =" << str2 << std::endl;

	std::cout << "str2.substr(5, 10); =" << str2.substr(5, 10) << std::endl;

	std::cin >> str2;
	std::cout << str2 << std::endl;
	getline(std::cin, str2);
	std::cout << str2 << std::endl;

	/*
	Алгоритм работы с файлом
	1. проверить существование файла (если существует, то открываем)
	2. создаем объект для работы с файлом
		а) открытие
		б) закрытие
		в) редактирование, запись, поиск, чтение
	Работа с файлами через функции ОС.
	*/

	std::fstream fs;
	fs.open("a.txt", std::fstream::out);
	fs << "qwertyuio asdfghjkl sdfghjkl dsfghjkl\n";
	fs.close();
	return 0;
}
