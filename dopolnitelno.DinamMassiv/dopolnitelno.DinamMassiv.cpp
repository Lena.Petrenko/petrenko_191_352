﻿// dopolnitelno.DinamMassiv.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
void EnterMassive(int * arr1, int size)
{
	int a = 0;
	while (a <= size)
	{
		std::cin >> arr1[a];
		a++;
	}
}

void SortMassive(int *arr1, int size )
{
	for (int c = 0; c < size; c++)
	{
		for (int k = c; k < size; k++)
		{
			if (arr1[c] > arr1[k])
			{
				int t;
				t = arr1[c];
				arr1[c] = arr1[k];
				arr1[k] = t;
			}
		}
	}
}
	void printMassive (int*mas, int size)
	{
		std::cout << std::endl;
		for (int i = 0; i < size; i++)
		{
			std::cout << "arr[" << i << "]= " << mas[i] << std::endl;
		}
	}

int main()
{
	int *arr;
	int size;
	std::cin >> size;
	arr = new int[size];
	EnterMassive(arr, size);
	SortMassive (arr, size);
	printMassive(arr, size);
	delete[] arr;
	return 0;
}

