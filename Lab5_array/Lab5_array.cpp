﻿// Lab5_array.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
void array_1(int * arr1, int nn)
{
	int a = 0;
	while (a <= nn)
	{
		std::cin >> arr1[a];
		a++;
	}
	for (int c = 0; c < nn; c++)
	{
		for (int k = c; k < nn; k++)
		{
			if (arr1[c] > arr1[k])
			{
				int t;
				t = arr1[c];
				arr1[c] = arr1[k];
				arr1[k] = t;
			}
		}
	}
}
void printarr(int*mas, int size)
{
	std::cout << std::endl;
	for (int i = 0; i < size; i++)
	{
		std::cout << "arr[" << i << "]= " << mas[i] << std::endl;
	}
}
int main()
{
	std::cout << "enter number" << std::endl;
	int n, a = 0;
	std::cin >> n;
	if (n > 100) std::cout << "error";
	int arr[100];
	array_1(arr, n);
	printarr(arr, n);
	return 0;
}