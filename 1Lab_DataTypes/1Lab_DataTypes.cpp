﻿// 1Lab_DataTypes.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>

int main()
{
	std::cout << "My name is Lena. I'm 18 years old. I like playing piano and listening music. I like literature and strikeball guns." << std::endl;
	double a, b, c, x;
	std::cout << "Enter a,b,c" << std::endl;
	std::cin >> a >> b >> c;
	x = (c - b) / a;
	std::cout << a << "x+" << b << "=" << c << std::endl;
	std::cout << "answer: x = " << x << std::endl;
	return 0;
}


