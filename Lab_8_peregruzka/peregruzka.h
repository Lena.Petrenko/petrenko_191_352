#pragma once
#include <iostream>
class matr
{
private:
	int  rows;
	int columns;
	int *arr;

public:
	matr();
	~matr();
	matr(int t_rows, int t_columns, int *mas);
	matr(matr&temp);
	matr& operator+=(const matr&mat);
	matr& operator -();
	friend matr& operator-(const matr & mat, const matr & mat2);//��� �������� -�������, -��� �������� �������*(-1) 
	friend matr& operator+(const matr & mat, const matr & mat2);//��� �������� -�������, -��� �������� �������*(-1) 
	friend std::ostream & operator << (std::ostream & out, const matr & mat);//��������
	friend std::istream & operator >> (std::istream & in, matr & mat);
	friend matr& operator*(const matr&mat, const int num);
	friend matr& operator*(const matr&mat, const matr&mat1);
	 matr& operator-=(const matr&mat);
};

inline matr & operator-(const matr & mat, const matr & mat2)
{
	int *res;
	res = new int[mat.columns*mat.rows];
	if ((mat.columns ==mat2.columns)&&(mat.rows == mat2.rows))
	{
		for (int i = 0; i < mat.columns*mat.rows; i++)
		{
			res[i] = mat.arr[i] - mat2.arr[i];
		}
		matr resMatr(mat.rows, mat.columns, res);
			return resMatr;
	}
}

inline matr & operator+(const matr & mat, const matr & mat2)
{

	int *res;
	res = new int[mat.columns*mat.rows];
	if ((mat.columns == mat2.columns) && (mat.rows == mat2.rows))
	{
		for (int i = 0; i < mat.columns*mat.rows; i++)
		{
			res[i] = mat.arr[i] + mat2.arr[i];
		}
		matr resMatr(mat.rows, mat.columns, res);
		return resMatr;
	}
}

inline std::ostream & operator<<(std::ostream & out, const matr & mat)
{
	
		out << "answer:" << std::endl;
		for (int i = 0; i < mat.rows; i++)
		{
			for (int j = 0; j < mat.columns; j++)
			{
				out << mat.arr[i*mat.columns + j] << "\t";
			}
			out << std::endl;
		}
		return out;
}
inline std::istream & operator>>(std::istream & in, matr & mat)
{
	std::cout<< "input rows" << std::endl;
	in >> mat.rows;
	std::cout << "input columns" << std::endl;
	in >>mat.columns;
	mat.arr = new int[mat.columns*mat.rows];
	std::cout << "input matrix" << std::endl;
	for (int i = 0; i < mat.columns*mat.rows; i++)
	{
		in >> mat.arr[i];
	}
	return in;
}
inline matr& operator *(const matr&mat, const int num)
{
	int *res;
	res = new int[mat.columns*mat.rows];
	for (int i=0; i < mat.columns*mat.rows; i++)
	{
		res[i]=mat.arr[i] * num;
	}
	matr resMatr(mat.rows, mat.columns, res);
	return resMatr;
}
inline matr&operator*(const matr&mat, const matr&mat1)
{
	if (mat.columns == mat1.rows)
	{
		int *res;
		res = new int[mat.columns*mat1.rows];
		/*int resrows = mat.rows;
		int rescolumns = mat1.columns;
		matr res(resrows, rescolumns);
*/
		for (int c = 0; c < mat.columns; c++)
			for (int r = 0; r < mat1.rows; r++)
			{
				int s = 0;
				for (int i = 0; i < mat.columns; i++)
					s += mat.arr[i + r * mat.columns] * mat1.arr[c + i * mat.columns];
				res[c + r * mat.columns] = s;

			}
		matr resMatr(mat1.rows, mat.columns, res);
		return resMatr;
	}
	else
	{
		int *res;
		res = new int[mat.columns*mat1.rows];
		matr resMatr(mat1.rows, mat.columns, res);
		for (int i = 0;i < mat.columns*mat1.rows; i++)
		{
			res[i] = 0;
		}
		return resMatr;
	}
}
