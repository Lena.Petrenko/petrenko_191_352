﻿// lab1_dataTypes.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

int main()
{
	std::cout << "My name is Lena. I'm 18 years old. I like playing piano, listening music. I like literature and films." << std::endl;
	return 0;
}
