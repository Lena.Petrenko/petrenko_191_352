#include "pch.h"
#include "matrix.h"
#include <iostream>

template<typename type>
void printMatrix(type* matr, int matr_col, int matr_rows)
{
	std::cout << "answer:" << std::endl;
	for (int i = 0; i < matr_rows; i++)
	{
		for (int j = 0; j < matr_col; j++)
		{
			std::cout << matr[i*matr_col + j] << "\t";
		}
		std::cout << std::endl;
	}
}
template<typename type>
matrix <type>::matrix()
{
}

template<typename type>
matrix <type>::~matrix()
{
}

template<typename type>
matrix <type>::matrix(int t_rows, int t_columns)
{
	rows = t_rows;
	columns = t_columns;
	arr = new int[rows*columns];
}
template<typename type>
matrix <type>::matrix(int t_rows, int t_columns, type *mas)
{
	rows = t_rows;
	columns = t_columns;
	arr = new type[rows*columns];
	for (int i = 0; i < (columns*rows); i++)
		arr[i] = mas[i];
}
template<typename type>
matrix <type>::matrix(matrix & temp)
{
	rows = temp.rows;
	columns = temp.columns;
	arr = new type[rows*columns];
	for (int i = 0; i < rows*columns; i++)
		arr[i] = temp.arr[i];
}
template<typename type>
matrix<type> & matrix<type>::MatrixSum(const matrix<type> & mat2)
{

		if (columns == mat2.columns && rows == mat2.rows)
		{
			int resrows = rows;
			int rescolumns = columns;
			matrix <type> res(resrows, rescolumns);
			//int*mat3=new int[resrows*rescolumns];
				
			for (int i = 0; i < resrows*rescolumns; i++)
				//mat3[i,j] = mat1[i, j]+mat2[i,j]
				res.arr[i] = arr[i] + mat2.arr[i];
			return res;
		}
		else
		{
			matrix <type> res(0, 0);
			return res;
		}
	// TODO: �������� ����� �������� return

}
template<typename type>
void matrix <type>::input()
{
	std::cout << "input size of matrix" << std::endl;
	std::cin >> rows;
	std::cin >> columns;
	arr = new type[rows*columns];
	std::cout << "enter munbers in array" << std::endl;
		for(int i=0; i<(columns*rows); i++)
		{
			std::cin >> arr[i];
		}
}
template<typename type>
void matrix<type>::print()
{
	printMatrix(arr, columns, rows);
}
template<typename type>
matrix<type>& matrix<type>::miltnum(int num)
{
	matrix res(rows, columns);
    //res = new int[rows*columns];
	for (int i = 0; i < rows*columns; i++)
	{
		res.arr[i] = arr[i] * num;
	}
	return res;
}
template<typename type>
matrix <type> matrix<type>::mult(const matrix<type> & mat2)
{
		if (columns == mat2.rows)
		{
			int resrows = rows;
			int rescolumns = mat2.columns;
			matrix<type> res(resrows, rescolumns);
		
			for (int c = 0; c < rescolumns; c++)
				for (int r = 0; r < resrows; r++)
				{
					int s = 0;
					for (int i = 0; i < columns; i++)
						s += arr[i + r * columns] * mat2.arr[c + i * mat2.columns];
					res.arr[c + r * rescolumns] = s;
				
				}
			return res;
		}
		else
		{
			matrix <type>res(0, 0);
			return res;
		}	
	}
template<typename type>
int matrix<type>::trace()
{
	//matrix matrix1(rows, columns);
	int res = 0;

	if (rows == columns)
	{
		int result = 0;
		for (int i = 0; i < columns; i++)
		{
			res += arr[i*columns + i];
		}
	}
	return res;
}
 