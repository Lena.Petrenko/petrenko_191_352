#pragma once
#include <iostream>
template<typename type>
void printMatrix(type* matr, int matr_col, int matr_rows);

template<typename type>
class matrix
{
private:
	int  rows;
	int columns;
	type *arr;
public:
	matrix();
	~matrix();
	matrix(int t_rows, int t_columns);
	matrix(int t_rows, int t_columns, type *mas);
	matrix(matrix<type>&temp);
    //sum
	matrix& MatrixSum(const matrix<type>& mat2);
	//multnum
	matrix& miltnum(int num);
	//mult
	matrix mult(const matrix<type>& mat2);
	//trace
	int trace();
	//det
	//print 
	void input();
	void print();


protected:
};

