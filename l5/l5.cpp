﻿#include "pch.h"
#include <iostream>
void sortbycounting(char* mas, int nn) 
{
	int counterarray[26] = { 0 };
	for (int i = 0; i < nn; i++) 
	{
		//'a' <=mas[i] <='z'
		//int('a')<= int(mas[i]) <= int('z')
		//int('a')-int('a') <= int(mas[i])-int('a') <= int('z') -int('a')
		// 0 <= int(mas[i])-int('a') <= 25
		counterarray[int(mas[i]) - int('a')]++;
	}
	int j = 0;//counter for mas;
	for (int i = 0; i < 26; i++) 
	{
		if (counterarray[i] > 0) 
		{
			for (int k = 0; k < counterarray[i]; k++)
			{
				mas[j] = char(int('a') + i);
				j++;
			}
		}
	}
}

void printarr(int* mas, int size) {
	std::cout << std::endl;
	for (int l = 0;l < size; l++) {
		std::cout << "arr[" << l << "]=" << mas[p] << std::endl;
	}
}

void princhararray(char* mas, int n) {
	for (int i = 0; i < n; i++) {
		std::cout << mas[i] << " ";
	}
	std::cout << std::endl;
}

void sort(int* massive, int size) {
	for (int c = 0; c < size; c++) {
		for (int k = c; k < size; k++) {
			if (massive[c] > massive[k]) {
				int t = massive[c];
				massive[c] = massive[k];
				massive[k] = t;
			}
		}
	}
	printarr(massive, size);

}

int digitsum(int n) 
{
	int res = 0;
	while (n > 0) 
	{
		res += n % 10;
		n = n / 10;
	}
	return res;
}

void sortingbysum(int* massive, int size) {
	for (int c = 0; c < size; c++) {
		for (int k = c; k < size; k++) {
			if (digitsum(massive[c]) < digitsum(massive[k]))
			{
				int g = massive[c];
				massive[c] = massive[k];
				massive[k] = g;
			}
		}
	}
	for (int i = 0; i < 5; i++) {
		cout << massive[i] << " ";
	}
	std::cout << std::endl;

}



void task1()
{
	int size = 0;
	char arr[100] = { ' ' };
	std::cout << "Input size of array: ";
	std::cin >> size;
	for (int i = 0; i < size; i++) {
		std::cin >> arr[i];
	}
	sortbycounting(arr, size);
	princhararray(arr, size);
}

void task2() {
	int arr[5];
	std::cout << "Input array of 5 numbers";
	for (int i = 0; i < 5; i++) {
		std::cin >> arr[i];
	}
	sortingbysum(arr, 5);
	for (int i = 0; i < 5; i++) {
		std::cout << arr[i];
	}

}



int main() {
	std::cout << "What do you want? \n"
		"1. Sort letters.\n"
		"2. Sort by sum of digits.\n"
		"3. Exit.\n";
	int task = 0;
	std::cin >> task;
	switch (task) 
	{
	case(1): 
	  {
		task1();
		break;
	  }

	case(2): 
	  {
		task2();
		break;
	  }

	default: 
	  {
		return 0; 
	  }
	}
}