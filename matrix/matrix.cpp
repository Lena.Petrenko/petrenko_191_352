﻿#include "pch.h"
#include<iostream>
void inputMatrix(int * &matr, int * matr_col, int * matr_rows)
{
	std::cout << "Input size of matrix (row, col): "<<std::endl;
	std::cin >> *matr_rows >> *matr_col;
	std::cout << "Input matrix's elements: \n";
	matr = new int[(*matr_col)*(*matr_rows)];
	for (int i = 0; i < (*matr_col)*(*matr_rows); i++)
		std::cin >> matr[i];
}

void printMatrix(int* matr, int matr_col, int matr_rows)
{
	std::cout << "answer:" << std::endl;
	for (int i = 0; i < matr_rows; i++)
	{
		for (int j = 0; j < matr_col; j++)
		{
			std::cout << matr[i*matr_col + j] << "\t";
		}
		std::cout << std::endl;
	}
}


void matrixsum(int * mat1, int mat1col, int mat1rows,
	int * mat2, int mat2col, int mat2rows,
	int * &mat3, int *mat3col, int *mat3rows)
{
	if (mat1col == mat2col && mat1rows == mat2rows)
	{
		*mat3rows = mat2rows;
		*mat3col = mat2col;
		mat3 = new int[mat2rows*mat2col];
		for (int i = 0; i < mat2rows*mat2col; i++)
			//mat3[i,j] = mat1[i, j]+mat2[i,j]
			mat3[i] = mat1[i] + mat2[i];
	}
	else
	{
		std::cout << "Error of sum: sizes are not equal.";
		*mat3col = *mat3rows = 0;
	}
}

void task1()
{

	int * matr1;
	int * matr2;
	int * matrres;
	int matr1_col, matr2_col, matrres_col;
	int matr1_rows, matr2_rows, matrres_rows;
	inputMatrix(matr1, &matr1_col, &matr1_rows);
	inputMatrix(matr2, &matr2_col, &matr2_rows);
	matrixsum(matr1, matr1_col, matr1_rows,
		matr2, matr2_col, matr2_rows,
		matrres, &matrres_col, &matrres_rows);

	printMatrix(matrres, matrres_col, matrres_rows);
	delete[] matr1;
	delete[] matr2;
	delete[] matrres;
}

void matrix_mul_num(int * mat1, int mat1col, int mat1rows, int num,
	int * &mat3, int *mat3col, int *mat3rows)
{
	*mat3rows = mat1rows;
	*mat3col = mat1col;
	mat3 = new int[mat1rows*mat1col];
	for (int i = 0; i < mat1rows*mat1col; i++)
		mat3[i] = mat1[i] * num;
}



void task2()
{

	int * matr1;
	int num;
	int * matrres;
	int matr1_col, matrres_col;
	int matr1_rows, matrres_rows;
	inputMatrix(matr1, &matr1_col, &matr1_rows);
	std::cout << "Enter the number: ";
	std::cin >> num;
	matrix_mul_num(matr1, matr1_col, matr1_rows, num,
		matrres, &matrres_col, &matrres_rows);

	printMatrix(matrres, matrres_col, matrres_rows);
	delete[] matr1;
	delete[] matrres;

}

void matrix_mul(int * mat1, int mat1col, int mat1rows,
	int * mat2, int mat2col, int mat2rows,
	int * &mat3, int *mat3col, int *mat3rows)
{
	if (mat1rows == mat2col)
	{
		*mat3rows = mat1rows;
		*mat3col = mat2col;
		mat3 = new int[mat1rows*mat2col];
		for (int c = 0; c < mat2col; c++)
			for (int r = 0; r < mat2col; r++)
			{
				int s = 0;
				for (int i = 0; i < mat1col; i++)
					s += mat1[i + r * mat1col] * mat2[c + i * mat2col];
				mat3[c + r * mat2col] = s;
				//s += mat1[i, r] * mat2[c, i];
				//mat3[c, r] = s;
			}
	}
	else
	{
		std::cout << "Error: Row1 <> Col2";
		*mat3col = *mat3rows = 0;
	}
}

void task3()
{

	int * matr1;
	int * matr2;
	int * matrres;
	int matr1_col, matr2_col, matrres_col;
	int matr1_rows, matr2_rows, matrres_rows;
	inputMatrix(matr1, &matr1_col, &matr1_rows);
	inputMatrix(matr2, &matr2_col, &matr2_rows);
	matrix_mul(matr1, matr1_col, matr1_rows,
		matr2, matr2_col, matr2_rows,
		matrres, &matrres_col, &matrres_rows);

	printMatrix(matrres, matrres_col, matrres_rows);
	delete[] matr1;
	delete[] matr2;
	delete[] matrres;

}

void matrix_sled(int * mat1, int mat1col, int mat1rows,
	int* num)
{
	if (mat1rows == mat1col)
	{
		int num = 0;

		for (int c = 0; c < mat1col; c++)
		{
			num += mat1[c + mat1col * c];
			if (c != (mat1col - c - 1))
				//num += mat1[c, mat1col-c-1];
				num += mat1[c + (mat1col*mat1col - c - 1)];

		}

	}
	else
	{
		std::cout << "Error: Row1 <> Col1";

	}
}

void task4()
{

	int * matr1;
	int num;
	int matr1_col;
	int matr1_rows ;
	inputMatrix(matr1, &matr1_col, &matr1_rows);

	matrix_sled(matr1, matr1_col, matr1_rows,
		&num);
	std::cout << "Sled: " << num;
	delete[] matr1;


}


int main() {
	std::cout << "What do you want? \n"
		"1. sum matrix.\n"
		"2. multiplicate matrix on number.\n"
		"3. multiplicate matrix on matrix.\n"
		"4. find sled of matrix.\n"
		"5. exit.\n";
	int task = 0;
	std::cin >> task;
	switch (task)
	{
	case(1):
	{
		task1();
		break;
	}

	case(2):
	{
		task2();
		break;
	}

	case(3):
	{
		task3();
		break;
	}
	case(4):
	{
		task4();
		break;
	}
	default:
	{
		return 0;
	}
	}
}