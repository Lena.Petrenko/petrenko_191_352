#include "pch.h"
#include "matrix.h"
#include <iostream>


void printMatrix(int* matr, int matr_col, int matr_rows)
{
	std::cout << "answer:" << std::endl;
	for (int i = 0; i < matr_rows; i++)
	{
		for (int j = 0; j < matr_col; j++)
		{
			std::cout << matr[i*matr_col + j] << "\t";
		}
		std::cout << std::endl;
	}
}


matrix::matrix()
{
}


matrix::~matrix()
{
}


matrix::matrix(int t_rows, int t_columns)
{
	rows = t_rows;
	columns = t_columns;
	arr = new int[rows*columns];
}
matrix::matrix(int t_rows, int t_columns, int *mas)
{
	rows = t_rows;
	columns = t_columns;
	arr = new int[rows*columns];
	for (int i = 0; i < (columns*rows); i++)
		arr[i] = mas[i];
}

matrix::matrix(matrix & temp)
{
	rows = temp.rows;
	columns = temp.columns;
	arr = new int[rows*columns];
	for (int i = 0; i < rows*columns; i++)
		arr[i] = temp.arr[i];
}

matrix & matrix::MatrixSum(const matrix & mat2)
{

		if (columns == mat2.columns && rows == mat2.rows)
		{
			int resrows = rows;
			int rescolumns = columns;
			matrix res(resrows, rescolumns);
			//int*mat3=new int[resrows*rescolumns];
				
			for (int i = 0; i < resrows*rescolumns; i++)
				//mat3[i,j] = mat1[i, j]+mat2[i,j]
				res.arr[i] = arr[i] + mat2.arr[i];
			return res;
		}
		else
		{
			matrix res(0, 0);
			return res;
		}
	// TODO: �������� ����� �������� return

}

void matrix::input()
{
	std::cout << "input size of matrix" << std::endl;
	std::cin >> rows;
	std::cin >> columns;
	arr = new int[rows*columns];
	std::cout << "enter munbers in array" << std::endl;
		for(int i=0; i<(columns*rows); i++)
		{
			std::cin >> arr[i];
		}
}

void matrix::print()
{
	printMatrix(arr, columns, rows);
}
matrix& matrix::miltnum(int num)
{
	matrix res(rows, columns);
    //res = new int[rows*columns];
	for (int i = 0; i < rows*columns; i++)
	{
		res.arr[i] = arr[i] * num;
	}
	return res;
}

matrix  matrix::mult(const matrix & mat2)
{
		if (columns == mat2.rows)
		{
			int resrows = rows;
			int rescolumns = mat2.columns;
			matrix res(resrows, rescolumns);
		
			for (int c = 0; c < rescolumns; c++)
				for (int r = 0; r < resrows; r++)
				{
					int s = 0;
					for (int i = 0; i < columns; i++)
						s += arr[i + r * columns] * mat2.arr[c + i * mat2.columns];
					res.arr[c + r * rescolumns] = s;
				
				}
			return res;
		}
		else
		{
			matrix res(0, 0);
			return res;
		}	
	}

int matrix::trace()
{
	//matrix matrix1(rows, columns);
	int res = 0;

	if (rows == columns)
	{
		int result = 0;
		for (int i = 0; i < columns; i++)
		{
			res += arr[i*columns + i];
		}
	}
	return res;
}
 