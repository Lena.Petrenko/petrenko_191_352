#include "pch.h"
#include "vector.h"
#include <iostream>
#include <math.h>
vector::vector()
{
	rows = 1;
}


vector::~vector()
{
}

vector::vector(int t_columns, int * t_arr)
{ 
	columns = t_columns;
	arr = new int[columns*rows];
	for (int i = 0; i < columns*rows; i++)
		arr[i] = t_arr[i];
}

vector::vector(int t_columns)
{
	columns = t_columns;
	arr = new int[columns*rows];
}

void vector::inputMatrix()
{
	std::cout << "input columns" << std::endl;
	std::cin >> columns;
	std::cout << "input matrix" << std::endl;
	arr = new int[columns*rows];
	for (int i = 0; i < (columns*rows); i++)
		std::cin >> arr[i];
}

//void vector::printMatrix()
//{
//	std::cout << "answer" << std::endl;
//	for (int i = 0; i < columns*rows; i++)
//		std::cout << arr[i]<<" ";
//	std::cout<<std::endl;
//}

double vector::lengthMatrix()
{
	std::cout << "length of Matrix = ";
	double res = 0;
	for (int i = 0; i < columns; i++)
	{
		res += arr[i] * arr[i];
	}
	std::cout << sqrt(res) << std::endl;
	return sqrt(res);
}

int vector::Mult(const vector & matr)
{
	int col = columns;
	int rows = 1;
	int res;
	res = 0;
	if (matr.columns == columns)
	{
		for (int i = 0; i < col; i++)
			{
				res += arr[i] * matr.arr[i];
			}
	}
	return res;
}

