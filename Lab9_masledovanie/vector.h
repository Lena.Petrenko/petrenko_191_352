#pragma once
#include "matrix.h"
#include <iostream>
class vector :
	public matrix
{
private: 
	//int columns;
	//int *arr;
	//int rows=1;

public:
	vector();
	~vector();
	vector(int t_colums, int *t_arr);
	vector(int t_columns);
	void inputMatrix();
	//void printMatrix();
	double lengthMatrix();
	int Mult(const vector &matr);
};

