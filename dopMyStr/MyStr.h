#pragma once
class MyStr
{
private:
	char str[256];
public:
	MyStr();
	~MyStr();
	MyStr(char *ch);
	int find(const char ch);
	int length();
	int compare(MyStr str1);
	void concat(MyStr str1);
public:

};

