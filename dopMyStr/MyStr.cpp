#include "pch.h"
#include "MyStr.h"


MyStr::MyStr()
{
}


MyStr::~MyStr()
{
}

MyStr::MyStr(char *ch)
{
	for (int i = 0; i < 256; i++)
		str[i] = ch[i];
}

int MyStr::find(const char ch)
{
	int i = 0;
	while ( str[i] != '\0')
	{
		if (str[i] == ch)
			return ch;
		else i++;
    }
	return -1;
}

int MyStr::length()
{
	int i = 0;
	while (str[i]!='\0')
	{ 
		i++;
	}
	return i;
}

int MyStr::compare(MyStr str1)
{
	int i = 0;
	while (str[i] != '\0')
	{
		if (str[i] < str1.str[i])
		{
			return -1;
		}
		if (str[i] > str1.str[i])
		{
			return 1;
		}
		i++;
	}
	if (str1.str[i] != '\0')
		return 1;
	else return 0;
}

void MyStr::concat(MyStr str1)
{
	int i = 0; int p = length();
	while (str1.str[i]!= '\0')
	{
		str[p + i] = str1.str[i];
		i++;
	}
	str[p + i] = '\0';
}

