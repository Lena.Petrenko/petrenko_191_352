﻿// map.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <map>
#include <string>
//void print(const std::string title, std::map<int, char> &mymap)
//{
//	std::cout << std::endl << title << ": " << std::endl;;
//	for (auto& a : mymap)
//	{
//		std::cout << a.first << " : " << a.second << std::endl;
//	}
//}
int main()
{
	int n,a;
	char b;
	std::map <int, char> mymap; /*{ {4561, 'l'}, {3754, 'a'}, {6511, 'c'}, {6787, 'd'} };*/
	std::cout  << "how much pairs? " << std::endl;
	std::cin >> n;
	std::cout << std::endl;
	std::cout << "enter the pair <int> <char>: ";
	for (int i = 0; i < n; i++)
	{
		std::cin >> a >> b; //
		mymap[a] = b;
	}
	std::map<int, char>::iterator it;
	for (it = mymap.begin(); it != mymap.end(); it++)
		std::cout << it->first << ' ' << it->second << '\n';
	std::cout << "what do you want to del?" << std::endl;
	std::cin >> a;
	for (auto it = mymap.begin(); it != mymap.end(); )
	{
		if (it->first == a)
			it = mymap.erase(it);
	}
	for (it = mymap.begin(); it != mymap.end(); it++)
		std::cout << it->first << ' ' << it->second << '\n';
}


