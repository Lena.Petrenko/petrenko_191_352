﻿// lab11_classwork.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "pet.h"
#include "Trad_pet.h"

int main()
{
	pet myPet;
	//myPat. <доступны только паблик>
	//протектед и прайвит не доступны!!!
	Trad_pet myTrad_pet;
	pet arr[2] = { myPet, myTrad_pet };
	myPet.Feed();
	myTrad_pet.Feed();
	arr[1].Celebrate();
	return 0;
}

