﻿#include"pch.h"
#include <cstring>
#include <sstream>
#include <string>
#include <iostream>

void countAndSay(int n)
{
	if (n == 0)
		std::cout <<"not num of row" << std::endl; 
	if (n == 1)
		std::cout<<"1"<< std::endl;
	std::string res = "1";
	std::cout<< res<< std::endl;
	std::string t_res;
	for (int i = 1; i < n; i++)
	{
		int dlina = res.length();
		for (int j = 0; j < dlina; j++)
		{
			int count = 1;
			while (j + 1 < dlina && res[j] == res[j + 1])
			{
				count++;
				j++;
			}

			t_res += std::to_string(count) + res[j];
		}
		res = t_res;
		std::cout << res <<std::endl;
		t_res = "";
	}   
}
 
int main()
{
	int n;
	std::cout << "enter number of rows"<<std::endl;
	std::cin >> n;
	countAndSay(n);
	return 0;
}